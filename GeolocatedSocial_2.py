import sys
from functools import reduce
from operator import add
from pyspark.sql import SparkSession


if __name__ == "__main__":
        if len(sys.argv) != 2:
                print("Fichier d'entree manquant");
                exit(-1)

        fileName = sys.argv[1]

        spark = SparkSession\
                .builder\
                .appName("Foursquare total nb users")\
                .getOrCreate()

        def splitFrequencies(x):
                tab = x.split(',')
                return ( tab[0],int(tab[1]) )

        text_file = spark.sparkContext.textFile(fileName)

        #voir 3.2 du rapport
        counts = text_file.flatMap(lambda l : l.split(' ')[1:]) \
                          .map(splitFrequencies) \
			  .map(lambda couple: (couple[0], [ couple[1] ] )) \
                          .reduceByKey(add) \
			  .map(lambda (a,b): (a, len(b))) \
			  .map(lambda (a,b): (b,a)) \
                          .sortByKey(False) \
                          .map(lambda (b,a): (a,b))


	output = counts.collect()
        print("Write results into geo_results_nb_users.txt...")
        resFile = open("geo_results_nb_users.txt","w")
        for (word, count) in output:
                resFile.write("%s: %i\n" %(word, count))

        resFile.close()
	spark.stop()
