import sys
from operator import add
from functools import reduce
from pyspark.sql import SparkSession
import time

if __name__ == "__main__":

        fileName = sys.argv[1] if len(sys.argv) > 1 else ""
        nbSlices = int(sys.argv[2]) if len(sys.argv) > 2 else None

        spark = SparkSession\
                .builder\
                .appName("foursquare moyenne nb viste par user")\
                .getOrCreate()
	start = time.time()
        def splitFrequencies(x):
                tab = x.split(',')
                return ( tab[0],int(tab[1]) )

	def computeMean(couple):
		sumValue = reduce(add, couple[1])
		return (couple[0], sumValue / len(couple[1]))

        text_file = spark.sparkContext.textFile(fileName, nbSlices)

        #voir 3.2 du rapport
        counts = text_file.flatMap(lambda l : l.split(' ')[1:]) \
                          .map(splitFrequencies) \
			  .map(lambda couple: (couple[0], [ couple[1] ] )) \
                          .reduceByKey(lambda accum, v: accum + v) \
			  .map(computeMean) \
			  .map(lambda (a,b): (b,a)) \
                          .sortByKey(False) \
                          .map(lambda (b,a): (a,b))

        output = counts.collect()
	print("Execution time : %s seconds" % (time.time() - start))
        print("Write results into geo_results_frequency.txt...")
        resFile = open("geo_results_frequency.txt","w")
        for (word, count) in output:
                resFile.write("%s: %i\n" %(word, count))

        resFile.close()
	spark.stop()
