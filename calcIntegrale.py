from pyspark import SparkContext
from pyspark.sql import SparkSession
from operator import add
from math import log
import time
import sys

spark = SparkSession\
        .builder\
        .appName("calcul integrale")\
        .getOrCreate()

def my_range(start,end,step):
        while start < end:
                yield start
                start += step

if __name__ == "__main__":
	start_time = time.time()
	nbRectangles = int(sys.argv[1]) if len(sys.argv) > 1 else 1
	nbSlices = int(sys.argv[2]) if len(sys.argv) > 2 else None
	borneMax = 10.0
	borneMin = 1.0

	longueur = borneMax - borneMin;
	longueurIntervalle = longueur / nbRectangles

    # cree une liste qui contient tous les x du calcul de l'approximation de l'integrale
	liste = my_range(borneMin, borneMax, longueurIntervalle)

	def integrale(elem):
		# 1 / x * length
		aire = longueurIntervalle / elem
		return aire 

	count = spark.sparkContext.parallelize(liste, nbSlices).map(integrale).reduce(add)

	print("Execution time : %s seconds" % (time.time() - start_time))
	print("log(x)[1,10] : %f" % (log(10) - log(1)) )
	print("Approximation de log(x)[1,10] %f" % count)

	spark.stop()
