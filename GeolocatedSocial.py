import sys
from operator import add

from pyspark.sql import SparkSession


if __name__ == "__main__":
	if len(sys.argv) != 2:
		print("Fichier d'entree manquant");
		exit(-1)

	fileName = sys.argv[1]

	spark = SparkSession\
		.builder\
		.appName("Foursquare total nb visite")\
		.getOrCreate()

	def splitFrequencies(x):
		tab = x.split(',')
		return (tab[0],int(tab[1]))

	text_file = spark.sparkContext.textFile(fileName)

	#voir 3.2 du rapport
	#l.split()[1:] : enleve l'id de l'utilisateur du resultat du split
	counts = text_file.flatMap(lambda l : l.split(' ')[1:]) \
		 	  .map(splitFrequencies) \
			  .reduceByKey(add) \
			  .map(lambda (a,b): (b,a)) \
			  .sortByKey(False) \
			  .map(lambda (b,a): (a,b))

	output = counts.collect()
	print("Write results into geo_results.txt...")
	resFile = open("geo_results.txt","w")
	for (word, count) in output:
		resFile.write("%s: %i\n" %(word, count))

	resFile.close()
	spark.stop()
